#!/bin/sh -e

#   GNU Octave data-structures package.
#   Copyright (C) 2013-2015  Daniel Kraft <d@domob.eu>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Build the package as .tar.gz file.

name=data-structures-0.1.0.tar.gz
dirname=data-structures

rm -rf /tmp/$dirname
mkdir /tmp/$dirname
cp COPYING DESCRIPTION INDEX /tmp/$dirname
cp -r inst/ /tmp/$dirname

# Clean up possible Vim swap files or something like that.
find /tmp/$dirname -type f -and -name .\* -exec rm -f {} \;

outfile=`pwd`/$name
(cd /tmp; tar zcvf $outfile $dirname)

rm -rf /tmp/$dirname
